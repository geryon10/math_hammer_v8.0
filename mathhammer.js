$('document').ready(function () {
    $("#num_attacks").change(function () {
        calculateDamage();
    });
    $("#to_hit").change(function () {
        calculateDamage();
    });
    $("#strength").change(function () {
        calculateDamage();
    });
    $("#extra_hits").change( function() {
        calculateDamage();
    })
    $("#damage").change(function () {
        calculateDamage();
    });
    $("#toughness").change(function () {
        calculateDamage();
    });
    $("#save").change(function () {
        calculateDamage();
    });
    $("#to_hit_reroll_no").change(function () {
        calculateDamage();
    });
    $("#to_hit_reroll_1").change(function () {
        calculateDamage();
    });
    $("#to_hit_reroll_miss").change(function () {
        calculateDamage();
    });
    $("#to_wound_reroll_no").change(function () {
        calculateDamage();
    });
    $("#to_wound_reroll_1").change(function () {
        calculateDamage();
    });
    $("#to_wound_reroll_miss").change(function () {
        calculateDamage();
    });
    $("#modifiers").change(function () {
        calculateDamage();
    });
    $("#auto_hit").change(function () {
        calculateDamage();
    })
    $("#ignore_wounds").change(function () {
        calculateDamage();
    });
    $("#tesla").change(function () {
        calculateDamage();
    });
    $("#var_attack").change(function () {
        calculateDamage();
    });
    $("#var_damage").change(function () {
        calculateDamage();
    });

    calculateDamage();
});

/**
 * will take the input of the form and calculate the expected wound total
 *
 * @return null
 */
function calculateDamage() {
    //  get the initial variables
    var intVarAttack = parseFloat($("#var_attack").val());
    var intVarDamage = parseFloat($("#var_damage").val());
    var intAttack = parseInt($("#num_attacks").val()) * intVarAttack;
    var intExtraAttack = parseInt($("#extra_hits").val()) * intVarAttack;
    var intHit = parseInt($("#to_hit").val());
    var intStrength = parseInt($("#strength").val());
    var intDamage = parseInt($("#damage").val()) * intVarDamage;
    var intTough = parseInt($("#toughness").val());
    var intSave = parseInt($("#save").val());
    var intModifier = parseInt($("#modifiers").val());
    var intIgnore = parseInt($("#ignore_wounds").val());

    //  get the initial radio buttons
    var bolToHitRerollOne = $("#to_hit_reroll_1").is(':checked');
    var bolToHitRerollMiss = $("#to_hit_reroll_miss").is(':checked');
    var bolToWoundRerollOne = $("#to_wound_reroll_1").is(':checked');
    var bolToWoundRerollMiss = $("#to_wound_reroll_miss").is(':checked');
    var bolAutoHit = $("#auto_hit").is(':checked');
    var bolTesla = $("#tesla").is(":checked");

    //  what the percent is to hit
    var fltPercentHit = (7 - intHit) / 6;

    var fltPerecentExtra = ( intAttack / 6 ) * intExtraAttack;

    //  what the estimated number of successful hits are
    var fltSuccessfulHits = ( intAttack * fltPercentHit ) + fltPerecentExtra;

    //  the to wound table based on strength and toughness
    var intToWound = 6;
    if (intStrength <= (intTough / 2)) {
        intToWound = 6;
    } else if (intStrength > (intTough / 2) && intStrength < intTough) {
        intToWound = 5;
    } else if (intStrength == intTough) {
        intToWound = 4;
    } else if (intStrength > intTough && intStrength < (intTough * 2)) {
        intToWound = 3;
    } else if (intStrength >= (intTough * 2)) {
        intToWound = 2;
    } else {
        intToWound = 6;
    }

    //  if there are rerolls, calculate the reroll successes
    var fltMisses = intAttack - fltSuccessfulHits;
    var fltReroll = 0;
    var fltRolls2Reroll = 0;

    if (bolToHitRerollOne == true) {
        fltRolls2Reroll = fltMisses * (1 / (intHit - 1));   //  need the amount of dice that rolled 1s (toHit of 4 1/3 of the dice would be 1s)
    } else if (bolToHitRerollMiss == true) {
        fltRolls2Reroll = fltMisses;
    }
    var fltMisses2Hits = fltRolls2Reroll * ((7 - intHit) / 6);

    fltSuccessfulHits += fltMisses2Hits;    //  add the new successes to the initial successes

    //  just incase there are more successful hits then attack, can't have that
    if (fltSuccessfulHits > intAttack) {
        fltSuccessfulHits = intAttack;
    }

    //  if there are misses and a modifier calculate the new successes
    fltMisses = intAttack - fltSuccessfulHits;

    //  after rerolls, calculate hits or misses after modifiers
    if (fltMisses > 0 && intModifier > 0) {
        var intModHit = 7 - intHit + intModifier;

        //  always succeed on 6's and always fail on 1's
        if (intModHit <= 0) {
            intModHit = 1;
        } else if (intModHit >= 6) {
            intModHit = 5;
        }
        fltSuccessfulHits += fltMisses * ((7 - intModHit) / 6);  //  add the successes/failures from the successful hits
    } else if (intModifier < 0) {
        //  after a -5 modifier only a roll of 6 would succeed, so we cannot go below a -5
        if (intModifier <= -5) {
            intModifier = -5;
        }

        //  calculate the new successful hits with the negative modifier
        var fltPercentFail = intModifier / 6;
        var fltNewFail = fltSuccessfulHits * fltPercentFail;
        fltSuccessfulHits += fltNewFail;
    }

    //  if the weapon has extra 3 hits on 6
    if (bolTesla == true && intModifier >= 0) {
        //  could reduce this all down to one line
        var intTeslaHit = 6 - intModifier;  //  tesla adds extra wounds on 6+, we add the modifier after the roll
        var fltPercentTesla = (7 - intTeslaHit) / 6;    //  gets the percent of tesla hits
        var fltTeslaExtraHits = fltSuccessfulHits * fltPercentTesla;
        fltSuccessfulHits += 2 * fltTeslaExtraHits;    //  add 2 extra hits for each tesla hit of 6+
    }

    //  if the weapon is an auto hit weapon, after all other calculations set to max
    if (bolAutoHit == true) {
        fltSuccessfulHits = intAttack;
    }
    $("#estimate_hits").text(fltSuccessfulHits);    //  output the estimated hits

    var fltSuccessfulWounds = (7 - intToWound) / 6;  //  calculate the amount of successful wound percent

    var fltWounds = fltSuccessfulHits * fltSuccessfulWounds;  //  calculate the wounds off successful hits

    //  if there are rerolls, calculate the reroll successes
    var fltNotWound = fltSuccessfulHits - fltWounds;
    var fltRerollWound = 0;
    var fltRolls2RerollWound = 0;

    //  the reroll to wounds
    if (bolToWoundRerollOne == true) {
        fltRolls2RerollWound = fltNotWound * (1 / (intToWound - 1));   //  need the amount of dice that rolled 1s (toHit of 4, 1/3 of the dice would be 1s)
    } else if (bolToWoundRerollMiss == true) {
        fltRolls2RerollWound = fltNotWound;
    }
    var fltMisses2Wounds = fltRolls2RerollWound * ((7 - intToWound) / 6);

    fltWounds += fltMisses2Wounds;

    //  if there is a save, calculate the percentage of saves
    var fltSave = 1;
    if (intSave == 7 || intSave == 0) {
        fltSave = 1;
    } else {
        fltSave = 1 - ((7 - intSave) / 6);
    }

    var fltTotalWounds = fltWounds * fltSave;

    if (intIgnore > 0) {
        fltTotalWounds = fltTotalWounds * (1 - ((7 - intIgnore) / 6));
    }

    $("#estimate_wounds").text(fltTotalWounds);

    var fltTotalDamage = fltTotalWounds * intDamage; //  calculate the final total damage

    $('#estimate_damage').text(fltTotalDamage);
}